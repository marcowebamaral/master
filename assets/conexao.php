<?php
try {
$conexao = new PDO('mysql:host=NOME_DO-SERVIDOR;dbname=NOME_BANCO_DADOS;charset=utf8','USUARIO_BANCO_DADOS','SENHA_USUARIO', array(PDO::MYSQL_ATTR_INIT_COMMAND=>'SET NAMES UTF8'));
$conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}  catch (PDOException $e) {
echo 'Erro de conexão:'.$e->getMessage().'<br>';
echo 'Código de erro:'.$e->getCode();
}
?>