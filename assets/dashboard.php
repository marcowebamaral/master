
<!doctype html>
<html ⚡>
<head>
  <title>Airportpark | Backend Test | Dashboard</title>
  <meta charset="utf-8">

<link  rel="stylesheet" type="text/css"  media="all" href="css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script></head>
  <!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="dashboard.php"><img src="images/logo.png" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <li><a href="categories.php" class="link-menu">Categorias</a></li>
      <li><a href="products.php" class="link-menu">Produtos</a></li>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="dashboard.php" class="link-logo"><img src="images/logo-car.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>    
</header>  
<!-- Header -->
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Dashboard</h1>
    </div>

    <div class="infor">
<?php
include 'conexao_local.php';
$consulta=$conexao->query("SELECT COUNT(id) AS total FROM produtos");
$exibe2=$consulta->fetch(PDO::FETCH_ASSOC);
$numLinhas = $exibe2['total'];
?>
      You have <?php echo "$numLinhas"; ?> products added on this store: <a href="addProduct.php" class="btn-action">Adicionar novo produto</a>&nbsp;&nbsp;<a href="formImportProd.php" class="btn-action">Importar produtos</a>
    </div>
    <ul class="product-list">
<?php
	$consulta2=$conexao->query("SELECT * FROM produtos ORDER BY id desc");
	while ($exibe=$consulta2->fetch(PDO::FETCH_ASSOC)) {	
?>

      <li style="padding:20px 30px; width:90%; align-items:center;">
        <div class="product-image">
          <img src="images/product/<?php echo $exibe['imagem']; ?>" layout="responsive" alt="foto do produto" style="width:100%;"/>
        </div>
        <div class="product-info">
          <div class="product-name"><p><span><?php echo $exibe['nome']; ?></span></p></div>
		  <?php
		  
		  ?>
          <div class="product-price"><span class="special-price"><?php echo $exibe['quantidade'];?> disponíveis</span> <span>R$
		  <?php
		  $precoFinal = number_format ($exibe['preco'], 2, ",", ".");
		  echo $precoFinal;
		  ?>
		  </span></div>
        </div>
      </li>
<?php
	}
?>
    </ul>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<footer>
	<div class="email-content">
	  <span>airportpark@airportpark.com.br</span>
	</div>
</footer>
 <!-- Footer --></body>
</html>
