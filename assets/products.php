
<!doctype html>
<html ⚡>
<head>
  <title>Airportpark | Backend Test | Products</title>
  <meta charset="utf-8">

<link  rel="stylesheet" type="text/css"  media="all" href="css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script></head>
  <!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="dashboard.php"><img src="images/logo.png" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <li><a href="categories.php" class="link-menu">Categorias</a></li>
      <li><a href="products.php" class="link-menu">Produtos</a></li>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="dashboard.php" class="link-logo"><img src="images/logo-car.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>    
</header>  
<!-- Header --><body>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Products</h1>
      <a href="addProduct.php" class="btn-action">Add new Product</a>
      <a href="formImportProd.php" class="btn-action">Import Products</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Imagem</span>
        </th>
		<th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Price</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantity</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categories</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
<?php
include 'conexao_local.php';
$consulta = $conexao->query("SELECT * FROM produtos ORDER BY id desc");
while ($exibe=$consulta->fetch(PDO::FETCH_ASSOC)) {
?>
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><img src="images/product/<?php echo $exibe['imagem']; ?>" width="40"></span>
        </td>
		<td class="data-grid-td">
           <span class="data-grid-cell-content"><?php echo $exibe['nome']; ?></span>
        </td>
      
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?php echo $exibe['sku']; ?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content">R$ 
		   <?php 
		   $precoFinal = number_format($exibe['preco'], 2, ",", ".");
		   echo $precoFinal;
		   ?>
		   </span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?php echo $exibe['quantidade']; ?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content">
		   <?php
   			$separaCat = explode('|', $exibe['categoria']);
			$numCat = count($separaCat);
			if($numCat>1) {
				foreach ($separaCat as $value) {
					$consulta2=$conexao->query("SELECT cat_nome FROM categorias WHERE id_cat='$value'");
					$exibe2=$consulta2->fetch(PDO::FETCH_ASSOC);
					echo $exibe2['cat_nome']."<br/>\n";
				}
			} else {
				$juntarCat = implode("",$separaCat);
				$consulta2=$conexao->query("SELECT cat_nome FROM categorias WHERE id_cat='$juntarCat'");
				$exibe2=$consulta2->fetch(PDO::FETCH_ASSOC);
				echo $exibe2['cat_nome'];
				}
			?>
			</span>
        </td>
        <td class="data-grid-td">
          <div class="actions">
            <div class="action edit"><span><a href="editaProd.php?id=<?php echo $exibe['id']; ?>">Edit</a></span></div>
            <div class="action delete"><span><a href="deletaProd.php?id=<?php echo $exibe['id']; ?>">Delete</a></span></div>
          </div>
        </td>
      </tr>
<?php
}
?>
    </table>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<footer>
	<div class="email-content">
	  <span>airportpark@airportpark.com.br</span>
	</div>
</footer>
 <!-- Footer --></body>
</html>
