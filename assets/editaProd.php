<!doctype html>
<html ⚡>
<head>
  <title>Airportpark | Backend Test | Add Product</title>
  <meta charset="utf-8">

<link  rel="stylesheet" type="text/css"  media="all" href="css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<meta name="viewport" content="width=device-width,minimum-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
<script src="jquery.mask.js"></script>

<script>
	
$(document).ready(function(){
    $("#price").mask("000.000.000.000,00", {reverse: true});
});
	
</script>
</head>
  <!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="dashboard.php"><img src="images/logo.png" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <li><a href="categories.php" class="link-menu">Categorias</a></li>
      <li><a href="products.php" class="link-menu">Produtos</a></li>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="dashboard.php" class="link-logo"><img src="images/logo-car.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>    
</header>  
<!-- Header -->
<?php
include 'conexao_local.php';
$id = $_GET['id'];
$consulta = $conexao->query("SELECT * FROM produtos WHERE id='$id'");
$exibe=$consulta->fetch(PDO::FETCH_ASSOC);
?>

  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Edit Product</h1>
    
    <form method="post" action="alteraProduto.php" name="alteraProduto" enctype="multipart/form-data">
      <div class="input-field">
	  <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">
        <label for="sku" class="label">Product SKU</label>
        <input type="text" id="sku" name="sku" class="input-text" value="<?php echo $exibe['sku']; ?>" required /> 
      </div>
      <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" id="name" name="name" class="input-text" value="<?php echo $exibe['nome']; ?>" required /> 
      </div>
      <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="text" id="price" name="price" class="input-text" value="<?php echo $exibe['preco']; ?>" required /> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="text" id="quantity" name="quantity" class="input-text" value="<?php echo $exibe['quantidade']; ?>" required /> 
      </div>
      <div class="input-field">
        <label for="category" class="label">Categories</label>
        <select multiple id="category" name="category[]" class="input-text" required>
		<?php
			$separaCat = explode("|", $exibe['categoria']);
			$num_cat_in = count($separaCat);
			$consulta3=$conexao->query("SELECT * FROM categorias");
			while($exibe3=$consulta3->fetch(PDO::FETCH_ASSOC)){
				$search_cat = in_array($exibe3['id_cat'], $separaCat, true);
				if($search_cat==true){
		?>
		<option value="<?php echo $exibe3['id_cat']; ?>" selected><?php echo $exibe3['cat_nome']; ?></option>
		<?php
				} else {
		?>
          <option value="<?php echo $exibe3['id_cat']; ?>"><?php echo $exibe3['cat_nome']; ?></option>
		<?php
				}
			}
		?>
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea id="description" name="description" class="input-text" required><?php echo $exibe['descricao']; ?></textarea>
      </div>
      <div class="input-field">
        <label for="imagem" class="label">Imagem do produto</label>
		<img src="images/product/<?php echo $exibe['imagem']; ?>" width="50">
        <input type="file" accept="image/*" id="imagem" name="imagem" class="input-text" />
		<input type="hidden" id="img" name="img" value="<?php echo $exibe['imagem']; ?>" />
      </div>
      <div class="actions-form">
        <a href="products.php" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Alterar" style="cursor:pointer;"/>
      </div>
      
    </form>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<footer>
	<div class="email-content">
	  <span>airportpark@airportpark.com.br</span>
	</div>
</footer>
 <!-- Footer --></body>
</html>
