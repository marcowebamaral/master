# Sobre o desenvolvimento para o teste
Como o projeto em questão é para testar os conhecimentos em PHP, não houve preocupação em alterar ou melhorar a interface gráfica das páginas.

Todo o processo foi montado tendo como base os arquivos em html e transformados em arquivos no formato PHP.

O teste completo foi montado em servidor próprio do estúdio e-bunny (do qual sou o proprietário) e pode ser acessado pelo link:
https://testairport.ebunny.com.br/dashboard.php

# Sobre o banco de dados
O banco de dados consiste em tabelas de produtos e tabelas de categorias.
As tabelas contém cada uma um campo **id** único que pode ser usado para criação de relações nas chamadas feitas ao banco de dados.

# Sobre a importação através de arquivos CSV
O arquivo **IMPORT.CSV** foi importado conforme solicitado no teste e os 1000 produtos ficam disponíveis no dashboard.

A importação foi montada tendo como base o formato CSV.

Pode ser feita de forma clara a rápida apenas usando o formulário de upload.

A importação segue o seguinte caminho:

- adiciona o produto da primeira linha do arquivo CSV na tabela de produtos (sem a adição de categorias);

- adiciona as categorias na tabela de categorias e atribui um código único hexadecimal a elas e também recolhe o **id único** daquela categoria;

- retorna à tabela de produtos e atualiza a linha correspondente ao produto da linha atual com o id da categoria recém adicionada;

- refaz o ciclo percorrendo todas as linhas do arquivo CSV.



# Sobre a inclusão de produtos e categorias
Sistema rápido e simples em que as informações são adicionadas via formulário e são levadas à uma página de controle para tratamento dos dados e inclusão no banco de dados.
**Informações pertinentes:**

- Na inclusão de produtos existe uma máscara em jquery para formato de preços colocados no campo correspondente;

- Na inclusão de produtos existe uma classe para redimensionamento das imagens colocadas somente para não permitir imagens muito grandes e garantir que a dashboard não fique muito pesada com imagens maiores que o aceitável para o desenvolvimemto web.

# Sobre a prática utilizada
Todo o teste, desde a conexão com o banco de dados, até os acessos às tabelas deste banco foram feitas utilizando orientação à objetos através de PDO (PHP Data Objects). Talvez a prática mais segura para troca de informações entre páginas e scripts dentro de um servidor.

# Obrigado pela oportunidade
Agradeço de antemão a oportunidade e espero corresponder às expectativas e poder agregar aos interessados.
